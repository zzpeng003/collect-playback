import pandas as pd
import haversine
from haversine import Unit, haversine
from numpy import double

# RoverTheoreticalVal = "./0329_ROVER_TheoreticalValue_No_NAV.txt"
RoverRealVal = "./0329_ROVER_RealValue_No_NAV.txt"
# RoverTheoreticalVal = "./theoratical0401.txt"   #第一次回放数据
# RoverRealVal = "./real0401.txt"
# RoverTheoreticalVal = "./theoratical0401+.txt"   #第一次回放数据加上.N文件
# RoverRealVal = "./real0401++.txt"
# RoverRealVal = "./playback_test2.txt"
RoverTheoreticalVal = "./theoratical0401+.txt"

class DataProcess():
    '''实现对比两个文档（实际和理论）中头三列的数据(time,latitude,longitude),
        将同一时间点的latitude和longitude取出，对比其误差
        文档需要预处理：
            1、将数据头前面的内容删除
            2、时间列保留时/分/秒，剔除年月日
    '''
    def GetTargetItem(self, FINENAME):
        '''提取txt文件中time、latitude、longitude三列的内容

        :param FINENAME:
        :return:
        '''
        content = []
        sub_item = []
        with open(FINENAME, "r") as data_txt:
            lines = data_txt.readlines()
            for line in lines:
                new_line = ' '.join(line.split())   #将文本中的多个空格合并成一个空格
                content.append(new_line)
            # print(content)
            for item in range(1, len(content)):
                item_content = content[item].split()
                new_item = item_content[0] + ' ' + item_content[1] + ' ' + item_content[2]
                sub_item.append(new_item)   # ['10:04:47.000 24.492845755 118.177198178', '10:04:48.000 24.492845653 118.177197951']
            # print(sub_item)
            return sub_item

    def GetTimeItem(self, target_list):
        '''获取txt文件中时间列表

        :param: target_list
        :return: txt文件中的时间列表
        '''
        sub_time = []
        for i in range(0, len(target_list)):
            TimeItem = target_list[i].split()[0]
            sub_time.append(TimeItem)
        return sub_time
            # print(TimeItem)
            # break

    def FindTargetIndex(self, theoretical_list, real_list):
        '''寻找两个文档中时间点一样的经纬度信息，并返回对应的index

        :param: theoretical_list，real_list
        :return: 目标index list
        '''
        IndexList = []
        for j in range(0, len(theoretical_list)):
            target = theoretical_list[j]
            for i in range(0, len(real_list)):
                if(target == real_list[i]):
                    SameTargetIndex = (j, i)
                    # print(SameTargetIndex)
                    IndexList.append(SameTargetIndex)
                    # print("理论值：")
                    # print(theoretical_list[j])
                    # print("实际值：")
                    # print(real_list[i])
        return IndexList

    def calculateErrors(self, lat1, lon1, lat2, lon2):
        point1 = (lat1, lon1)
        point2 = (lat2, lon2)
        # 两点的经纬度
        result = haversine(point1, point2, unit=Unit.METERS)  # m
        return result

    def GetErrorResults(self, TheoreticalTimeList, RealTimeList, RoverTheoreticalData, RoverRealData):
        '''对比两个文档中time相等时经纬度信息，计算误差，返回误差列表

        :param:
                TheoreticalTimeList：理论内容文档中时间戳列表
                RealTimeList：实际内容文档中时间戳列表
                RoverTheoreticalData：理论内容文档中所有内容列表
                RoverRealData：：实际内容文档中所有内容列表
        :return: 两个文档中经纬度信息比较结果误差列表
        '''
        result_list = []
        TargetIndex = self.FindTargetIndex(TheoreticalTimeList, RealTimeList)
        for index in range(0, len(TargetIndex)):
            print("第{}组数据".format(index))
            sub_index = TargetIndex[index]   # 输出['0','82']
            theoratical_index = sub_index[0]    # 理论值索引
            real_index = sub_index[1]   # 实际值索引
            current_real_content = RoverRealData[real_index].split()
            real_lat = double(current_real_content[1])
            print("实际经度：{}".format(real_lat))
            real_lon = double(current_real_content[2])
            print("实际纬度：{}".format(real_lon))
            current_theo_content = RoverTheoreticalData[theoratical_index].split()
            theo_lat = double(current_theo_content[1])
            print("理论经度：{}".format(theo_lat))
            theo_lon = double(current_theo_content[2])
            print("理论纬度：{}".format(theo_lon))
            result = self.calculateErrors(real_lat, real_lon, theo_lat, theo_lon)
            print("误差结果：{}".format(result))
            result_list.append(result)
            print('\n')
        return result_list

    def SingleTextErrorCalculation(self, TextData):
        result_list = []
        for FormerItem in range(0, len(TextData) - 1):
            Former_lat_Info = double(TextData[FormerItem].split()[1])
            Former_lon_Info = double(TextData[FormerItem].split()[2])
            Later_lat_Info = double(TextData[FormerItem + 1].split()[1])
            Later_lon_Info = double(TextData[FormerItem + 1].split()[2])
            result = self.calculateErrors(Former_lat_Info, Former_lon_Info, Later_lat_Info, Later_lon_Info)
            result_list.append(result)
        return result_list

    def main(self):
        result_list = []
        print("理论值：")
        RoverTheoretical = self.GetTargetItem(RoverTheoreticalVal)
        print(RoverTheoretical)
        print(len(RoverTheoretical))
        RoverTheoreticalTimeList = self.GetTimeItem(RoverTheoretical)
        print(RoverTheoreticalTimeList)

        print("实际值")
        RoverReal = self.GetTargetItem(RoverRealVal)
        print(RoverReal)
        RoverRealTimeList = self.GetTimeItem(RoverReal)
        print(RoverRealTimeList)
        print('\n')

        result_list = self.SingleTextErrorCalculation(RoverReal)
        # result_list = self.GetErrorResults(RoverTheoreticalTimeList, RoverRealTimeList, RoverTheoretical, RoverReal)
        print("误差结果列表：{}".format(result_list))
        print(len(result_list))

        # 计算误差平均值
        sum = 0
        for item in range(0, len(result_list)):
            sum += result_list[item]
        average = sum / len(result_list)
        print("误差平均值：{}米".format(average))
            # print(current_theo_content)
            # print(type(current_theo_content))
            # break


if __name__ == '__main__':
    Goal = DataProcess()
    Goal.main()
    '''
    输入target，返回target所对应的longitude和latitude
    '''
